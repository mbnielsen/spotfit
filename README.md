This is a repository for spotfit, which is a Bayesian fitting code using a parallelized MCMC sampler. The spot model used in this code is based on macula (Kipping, 2012). 

Setup:
Compile the macula.f90 file using f2py with something like:
f2py --f90exec=gfortran --opt='-O3' -c macula.f90 -m macula

Perform a fit with:
python spotfit.py /path/to/time_series.dat

The default settings launches a short test fit to check that everything is working. For full use the following options are available:
-spots    : sets the number of spots to be included in the fits, initial guesses are randomized for each spot
-nsteps   : number of steps for the MCMC sampler to take
-nwalkers : number of walkers that the MCMC sampler should use
-burnt    : use a previous run as starting location
-store    : store result
-fig      : show plot of best fit
