import sys, emcee, argparse
sys.path.append('/home/martin/work/repos/pyfuncs')
from miscfuncs import bin_data
from numpy import *
from matplotlib.pyplot import *
from macula import maculamod
import numba as nb
from time import time as tm


def bin_data(X, Y, E = array([]), xmin = None, xmax = None, nbins = 10, wrap = False):

    x, y, e = X.copy(), Y.copy(), E.copy()

    if not xmin:
        xmin = min(x)
    if not xmax:
        xmax = max(x)
    
    width = (xmax - xmin)/float(nbins)

    BD = {}

  
    BCs = (arange(1, nbins + 1)-1./2)*width + xmin

    BD['bins']        = {str(p): array([])      for p in range(len(BCs))}
    BD['bins_err']    = {str(p): array([])      for p in range(len(BCs))}
    BD['bin_centers'] = {str(p): BCs[p]         for p in range(len(BCs))}

    for i in range(len(BCs)):
        
        idx = (x >= BCs[i] - width/2) * (x <= BCs[i] + width/2)

        BD['bins'][str(i)]     = y[idx]
        BD['bins_err'][str(i)] = e[idx]
               
    
    N = len(BD['bin_centers'].keys())

    bin_centers, bin_means, bin_std = zeros(N),zeros(N),zeros(N)
    for k in range(N):

        if len(BD['bins'][str(k)]) > 0:
        
            bin_centers[k] = BD['bin_centers'][str(k)]
            
            bin_means[k]   = average(BD['bins'][str(k)], weights = 1./BD['bins_err'][str(k)])

            bin_std[k]     = std(BD['bins'][str(k)]) / sqrt(len(BD['bins'][str(k)]))
        else:
            continue


    sidx = argsort(bin_centers)

    bin_centers, bin_means, bin_std = bin_centers[sidx],bin_means[sidx],bin_std[sidx]

    return bin_centers, bin_means, bin_std


def spotmodel(theta, x, LD, n, theta_inst, tstart, tend):
    
    theta_star = asfortranarray(append(theta[:4],LD))

    theta_spot = array(theta[5:]).reshape((8,n), order = 'F')

    model = maculamod(x,
                      theta_star,
                      theta_spot,
                      theta_inst,
                      tstart,
                      tend)

    return model


def lnprob(theta, t, d, e, LD, Nspots, theta_inst, tstart, tend, lim, C):
    return maculamod.lnprobability(theta, t, d, e, LD, Nspots, theta_inst, tstart, tend, lim, C)

def identify_input():

    p = argparse.ArgumentParser(description = """""",
                                epilog = """""")

    # Time series
    p.add_argument('path'    , help = """full path to time series"""        , default = None, type = str)
    p.add_argument('-nspots' , help = """number of spots to include in the fit""", default = 1, type = int)
    p.add_argument('-nsteps' , help = """number of steps for the MCMC walkers to take""", default = 10, type = int)
    p.add_argument('-nwalkers', help = """number of MCMC walkers to use""", default = None, type = int)
    p.add_argument('-burnt' , help = """Continue MCMC run based on previous run""", action = 'store_true')
    p.add_argument('-store' , help = """Save output from MCMC run""", action = 'store_true')
    p.add_argument('-fig', help = """Show plot of best-fit""", action = 'store_true')
    
    return  p.parse_args()


settings = identify_input()


#+++++++++++++++++++++++++++++++

nsteps = settings.nsteps
spread = 0.5

#+++++++++++++++++++++++++++++++


T,D,E = genfromtxt(settings.path).T

idx = invert(isnan(D)|isinf(D))

T,D,E = T[idx],D[idx],E[idx]

T -= T[0]

tspan = T[-1]-T[0]

t, d, e = bin_data(T,D,E,xmin = T[0],xmax = T[-1], nbins = 3*int(tspan))

# zero values are offset
e[e == 0] += median(e) 
d[d == 0] += 1e-20

d *= 1e-6
e *= 1e-6

d -= max(d)-1

fac = pi/180

# Initial guesses 
theta = [86.*fac,       #incl
         19.,           #Peq
         0.3,           #Palpha
         0.3,           #Pbeta
         0.5,           #lnf (error scale parameter)
         1*fac,         #spot lat
         1*fac,         # spot lon
         2*fac,         # spot alpha_max
         0.3,           # Fspot/Fstar
         0.5*tspan,     # tau_max
         0.1*tspan,     # lifetime
         0.1*tspan,     # ingress
         0.1*tspan,     # egress
         ]              

# Uniform priors (fit limits)
lim = array([[(1e-10)*fac,    (90-1e-10)*fac],            #incl
             [1,              45],                        #Peq
             [0,              10],                        #Palpha
             [0,              10],                        #Pbeta
             [-50,             5],                        #lnf (error scale parameter)
             [(1e-10)*fac,    (90-1e-10)*fac],            #spot lat
             [(1e-10)*fac,    (360-1e-10)*fac],           #spot long
             [0.1*fac,        10*fac],                    #spot alpha_max
             [1e-10,          1-1e-10],                   #Fspot/Fstar
             [-0.1*tspan,         1.1*tspan],             #tau_max
             [1,              3*tspan],                   #lifetime
             [1,              tspan],                     #growth time
             [1,              tspan]])                    #decay time
       

# Expanding arrays to adjust for requested spot number (some parameters are randomonized)
theta_spot = theta[5:]

lim_spot = lim[5:,:]

for n in range(settings.nspots-1):

    for k in [0,1,4]:
        theta_spot[k] = random.uniform(lim_spot[k,0],lim_spot[k,1])
    
    theta = append(theta, theta_spot)

    lim = vstack((lim,lim_spot))

ndim= len(theta)

if settings.nwalkers == None:
    nwalkers = 20 * (len(theta)+1)
else:
    nwalkers = settings.nwalkers
    

# Load in previous run? Start positions for MCMC will be end positions of previous run.
if settings.burnt:
    C = load('spotfit.chain.npz')['a']
    
    pos = C[:,-1,:]

else:
    pos = np.array([[random.uniform(max(lim[i,0], theta[i]*(1.0-spread)),
                                    min(lim[i,1], theta[i]*(1.0+spread))) for i in range(len(theta))] for k in range(nwalkers)])

# Set limb-darkening coefficients LD[:4] are for the star, LD[4:] are for the spots
LD = array([0.3999, 0.4269, -0.0227, -0.839, 0.3999, 0.4269, -0.0227, -0.839], order = 'F')

# Instrumental parameters for macula
theta_inst = array([1,1], order = 'F')


tstart = t[0]-0.01
tend = t[-1]+0.01


theta = asfortranarray(theta)
t,d,e = asfortranarray(t),asfortranarray(d),asfortranarray(e)
lim = asfortranarray(lim)

# Likelihood offset
C = -0.5*len(t)*log(2*pi)-0.5*sum(log(e**2))

sampler = emcee.EnsembleSampler(nwalkers, ndim, lnprob, args=(t, d, e, LD,
                                                              settings.nspots,
                                                              theta_inst,
                                                              tstart, tend,
                                                              lim, C), threads = 4)

t1 = tm()
sampler.run_mcmc(pos, settings.nsteps)
t2 = tm()-t1
print 'Total time:',t2
print 'Aproxx. time per logL evaluation:',t2/(settings.nsteps*nwalkers)

# Store chains and likelihood
if settings.store:
    savez('spotfit.chain.npz', a = sampler.chain)
    savez('spotfit.lnlike.npz', a = sampler.lnprobability)

# Show plot of best fit (based on median)
if settings.fig:
    
    chain = sampler.chain[:, -settings.nsteps:, :]
   
    samples = chain.reshape((-1, ndim))
    
    bf = median(samples,axis = 0)
 
    m = spotmodel(bf, t, LD, settings.nspots, theta_inst, tstart, tend)
    
    fig = figure()
    ax = fig.add_subplot(111)
    ax.plot(t,d,'.', label = 'Data')
    ax.plot(t,m,'.', label = 'Model')
    ax.set_xlabel('Time [days]')
    ax.set_ylabel('Relative flux')
    legend()
    show()
    
sys.exit()






















##
##
##def lnL(theta, x, y, yerr, LD, n, theta_inst, tstart, tend):
##
##    theta_star = asfortranarray(append(theta[:4],LD))
##
##    theta_spot = array(theta[5:]).reshape((8,n), order = 'F')
##
##    model = maculamod(x,
##                      theta_star,
##                      theta_spot,
##                      theta_inst,
##                      tstart,
##                      tend)
##    
##    return lnlikelihood(y,yerr,model,theta[4])
##
##def lnprior(theta,lim):
##
##    lnp = 0.0
##    for i in range(len(theta)):
##        if lim[i,0] < theta[i] < lim[i,1]:
##            continue
##        else:
##            return -inf
##    return lnp
##    
##def lnprob(theta, x, y, yerr, LD, n, theta_inst, tstart, tend, lim):
##    lp = lnprior(theta,lim)
##    if not isfinite(lp):
##        return -inf
##    return lp + lnL(theta, x, y, yerr, LD, n, theta_inst, tstart, tend)
##
##
##
##
##
##
##dt = spotmodel(theta,t,LD,Nspots,theta_inst,tstart,tend)+random.randn(len(t))*1e-4
##
##fig1 = figure()
##ax1 = fig1.add_subplot(111)
###ax1.plot(t,dt)
##
##fig2 = figure()
##ax2 = fig2.add_subplot(111)
##
##
##
##for i in arange(10,40,0.5):
##
##    theta[1] = i
##    
##    m = spotmodel(theta,t,LD,Nspots,theta_inst,tstart,tend)
##    
##    if theta[1] == 25.0:
##        ax1.plot(t,dt)
##        ax1.plot(t,m)
##        
##    p = lnprob(theta, t, dt, e, LD, Nspots, theta_inst, tstart, tend, lim,C)
##    
##    ax2.plot(i,p,'.')
##
##show()
##sys.exit()
##
##
##
##star_parlabs = ['incl','Peq','kappa2','kappa4']
##LD_parlabs =   ['c1','c2','c3','c4','d1','d2','d3','d4']
##spot_parlabs = ['lat','lon','alpha_max','contrast', 'tmax', 'lifetime', 'ingress', 'egress']    
##
##
##    spotd = {x: theta[i*n+4:(i+1)*n+4] for i,x in enumerate(spot_parlabs)}
##
##    stard.update({x:LD[i] for i,x in enumerate(LD_parlabs)})
##
##    print spotd
##    model = MaculaModel(t = x, nspots = n, star = stard, spots = spotd)
##
##    return model(x)
##    
#t, derivatives, temporal, tdeltav, theta_star, theta_spot, theta_inst, tstart, tend) 
##
##samples = sampler.chain[:, 0:, :].reshape((-1, ndim))
##
##
##print shape(samples)
##
##figure()
##plot(sampler.chain[:,:,2].T,'.', color = 'k', ms = 1, alpha = 0.2)
##show()
