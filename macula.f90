! ==============================================================================
! 				MACULA version 1.3			       !
! ==============================================================================
!
! AUTHOR: David Kipping
!         Harvard-Smithsonian Center for Astrophysics
!         Please report any problems to: dkipping@cfa.harvard.edu
!
! CITATION: If using this code, please cite:
!            Kipping, D. M., 2012, 'An Analytic Model for Rotational Modulations
!            in the Photometry of Spotted Stars', MNRAS, 427, 2487
!
! DESCRIPTION: MACULA computes the photometric flux variations due to starspots
!              on the surface of a rotating star. Star is assumed to be
!              spherical and exhibit four-coefficient non-linear limb darkening.
!              Starspots are circular, gray and exhibit four-coefficient
!              non-linear limb darkening too. MACULA allows for multiple 
!              starspots which are assumed to not overlap. Differential
!              rotation and starspot evolution is included. Partial derivatives
!              are provided of the model flux with respect to the model inputs,
!              as well as time. Finally, expected transit depth variations due
!              to unocculted spots is evaluated.
!
! CHANGES: v1.1 - Corrected an error in the calculation of TdeltaV's
!          v1.2 - Corrected a bug with tref (thanks to Bence Beky)
!          v1.3 - Several minor speed improvements: converted heaviside 
!                 functions to 'if' statements, some trig replacements, complex 
!                 functions removed and replaced using 'if' statements.
!                 Yields ~100% faster execution times with derivatives off
!                 Yields ~20% faster execution times with derivatives on
!              
! INPUTS:
!
! ------------------------------------------------------------------------------
! Theta_star(j) = Parameter vector for the star's intrinsic parameters
! ------------------------------------------------------------------------------
! Istar 	= Theta_star(1)		! Inclination of the star [rads]
! Peq 		= Theta_star(2)		! Rot'n period of the star's equator [d]
! kappa2 	= Theta_star(3)		! Quadratic differential rotation coeff
! kappa4 	= Theta_star(4)		! Quartic differential rotation coeff
! c1 		= Theta_star(5)		! 1st of four-coeff stellar LD terms
! c2 		= Theta_star(6)		! 2nd of four-coeff stellar LD terms
! c3 		= Theta_star(7)		! 3rd of four-coeff stellar LD terms
! c4 		= Theta_star(8)		! 4th of four-coeff stellar LD terms
! d1 		= Theta_star(9)		! 1st of four-coeff spot LD terms
! d2	 	= Theta_star(10)	! 2nd of four-coeff spot LD terms
! d3	 	= Theta_star(11)	! 3rd of four-coeff spot LD terms
! d4	 	= Theta_star(12)	! 4th of four-coeff spot LD terms
! ------------------------------------------------------------------------------
! Theta_spot(j,k) = Parameters of the k^th spot
! ------------------------------------------------------------------------------
! Lambda0(k) 	= Theta_spot(1,k)	! Longitude of spot at time tref(k)
! Phi0(k) 	= Theta_spot(2,k)	! Latitude of spot at time tref(k)
! alphamax(k)	= Theta_spot(3,k)	! Angular spot size at time tmax(k)
! fspot(k)	= Theta_spot(4,k)	! Spot-to-star flux contrast of spot k
! tmax(k)	= Theta_spot(5,k)	! Time at which spot k is largest
! life(k)	= Theta_spot(6,k)	! Lifetime of spot k (FWFM) [days]
! ingress(k)	= Theta_spot(7,k)	! Ingress duration of spot k [days]
! egress(k)	= Theta_spot(8,k)	! Egress duration of spot k  [days]
! ------------------------------------------------------------------------------
! Theta_inst(j,m) = Instrumental/nuisance parameters
! ------------------------------------------------------------------------------
! U(m) 		= Theta_inst(1,m)	! Baseline flux level for m^th data set
! B(m) 		= Theta_inst(2,m)	! Blend factor for m^th data set
! ------------------------------------------------------------------------------
! Non-fitted input parameters
! ------------------------------------------------------------------------------
! Tstart(m)				! Time stamp @ start of m^th data series 
! Tend(m)				! Time stamp @ end of m^th data series
! t(i)					! Time stamp of i^th data point [days]
! ndata					! Number of data points
! mmax					! Number of data sets
! Nspot					! Number of starspots (over all times)
! ------------------------------------------------------------------------------
!
! OUTPUTS:
!
! Fmod(i)				! Model flux
! ==============================================================================

MODULE maculamod

implicit none

CONTAINS

! ==============================================================================
! ============================= SUBROUTINE: MACULA =============================
! ==============================================================================

SUBROUTINE macula(t,ndata,Nspot,mmax,& !Controls
                  Theta_star,Theta_spot,Theta_inst,&	  !System parameters
                  Tstart,Tend,&				  !Data start/end times
                  Fmod)!Outs

implicit none
 ! === PRE-AMBLE ===
 ! Note - There should be no need to ever change these three parameters.
 INTEGER, PARAMETER :: pstar = 12
 INTEGER, PARAMETER :: pspot = 8
 INTEGER, PARAMETER :: pinst = 2
 ! === INPUTS ===
 REAL(8), DIMENSION(ndata), INTENT(IN) :: t
 REAL(8), DIMENSION(pstar), INTENT(IN) :: Theta_star
 REAL(8), DIMENSION(pspot,Nspot), INTENT(IN) :: Theta_spot
 REAL(8), DIMENSION(pinst,mmax), INTENT(IN) :: Theta_inst
 REAL(8), DIMENSION(mmax), INTENT(IN) :: Tstart, Tend
 ! === VARIABLES ===
 INTEGER :: i, j, k, l, m, n
 INTEGER, INTENT(IN) :: ndata, mmax, Nspot
 INTEGER :: jmax
 REAL(8), DIMENSION(Nspot) :: tref ! By default, macula will set tref(k)=tmax(k)
 REAL(8) :: SinInc, CosInc
 REAL(8), PARAMETER :: pi = 3.141592653589793D0
 REAL(8), PARAMETER :: halfpi = 1.5707963267948966D0
 REAL(8), PARAMETER :: piI = 0.3183098861837907D0
 REAL(8), PARAMETER :: tol = 1.0D-4 ! alpha values below this will be ignored
 REAL(8), PARAMETER :: mingress = 0.0416667D0 ! minimum ingress/egress time allowed
 ! === SECTION 1 ===
 REAL(8), DIMENSION(pstar+pspot*Nspot+pinst*mmax) :: Theta
 ! === SECTION 2 ===
 REAL(8), DIMENSION(5) :: c, d
 REAL(8), DIMENSION(mmax) :: U, B
 REAL(8), DIMENSION(mmax,ndata) :: Box
 REAL(8), DIMENSION(Nspot) :: Phi0, SinPhi0, CosPhi0, Prot
 REAL(8), DIMENSION(Nspot,ndata) :: beta, sinbeta, cosbeta
 REAL(8), DIMENSION(Nspot,ndata) :: alpha, sinalpha, cosalpha
 REAL(8), DIMENSION(Nspot,ndata) :: Lambda, sinLambda, cosLambda
 REAL(8), DIMENSION(Nspot) :: tcrit1, tcrit2, tcrit3, tcrit4
 REAL(8), DIMENSION(Nspot) :: alphamax, fspot, tmax, life, ingress, egress
 ! === SECTION 3 ===
 REAL(8), DIMENSION(Nspot,ndata) :: zetaneg, zetapos
 REAL(8), DIMENSION(5,Nspot,ndata) :: Upsilon, w
 REAL(8), DIMENSION(Nspot,ndata) :: Psi, Xi
 REAL(8), DIMENSION(Nspot,ndata) :: q, A
 REAL(8) :: Fab0
 REAL(8), DIMENSION(ndata) :: Fab
 REAL(8), DIMENSION(ndata), INTENT(OUT) :: Fmod


 ! ===================================
 ! === SECTION 1: THETA ASSIGNMENT ===
 ! ===================================

 jmax = pstar + pspot*Nspot + pinst*mmax

 l = 0
 DO j=1,pstar
   l = l + 1
   Theta(l) = Theta_star(j)
 END DO
 DO k=1,Nspot
   DO j=1,pspot
     l = l + 1
     Theta(l) = Theta_spot(j,k)
   END DO
 END DO
 DO j=1,pinst
   DO m=1,mmax
     l = l + 1
     Theta(l) = Theta_inst(j,m)
   END DO
 END DO

 ! Thus we have...
 !Theta_star(j) = Theta(j)
 !Theta_spot(j,k) = Theta(pstar + pspot*(k-1) + j)
 !Theta_inst(m) = Theta(pstar + pspot*Nspot + m)

 ! ===================================
 ! === SECTION 2: BASIC PARAMETERS ===
 ! ===================================

 ! c and d assignment
 c(2) = Theta(5); c(3) = Theta(6); c(4) = Theta(7); c(5) = Theta(8)
 d(2) = Theta(9); d(3) =Theta(10); d(4)= Theta(11); d(5)= Theta(12)
 c(1) = 1.0D0 - c(2) - c(3) - c(4) - c(5) !c0
 d(1) = 1.0D0 - d(2) - d(3) - d(4) - d(5) !d0

 ! inclination substitutions
 SinInc = DSIN(Theta(1))
 CosInc = DCOS(Theta(1))

 ! U and B assignment
 DO m=1,mmax
   U(m) = Theta(pstar+pspot*Nspot+m)
   B(m) = Theta(pstar+pspot*Nspot+mmax+m)
 END DO

 ! Box-car function (labelled as Pi_m in the paper)
 DO i=1,ndata
   DO m=1,mmax
     IF( t(i) .GT. Tstart(m) .AND. t(i) .LT. Tend(m) ) THEN
       Box(m,i) = 1.0D0
     ELSE
       Box(m,i) = 0.0D0
     END IF
   END DO
 END DO

 ! Phi0 & Prot calculation
 DO k=1,Nspot
   Phi0(k) = Theta(pstar+pspot*(k-1)+2)
 END DO
 DO k=1,Nspot
   SinPhi0(k) = DSIN(Phi0(k))
   CosPhi0(k) = DCOS(Phi0(k))
   Prot(k) = Theta(2)/(1.0D0-Theta(3)*SinPhi0(k)**2-Theta(4)*SinPhi0(k)**4)
 END DO

 ! alpha calculation
 DO k=1,Nspot
   alphamax(k) = Theta(pstar + pspot*(k-1) + 3)
   fspot(k) = Theta(pstar + pspot*(k-1) + 4)
   tmax(k) = Theta(pstar + pspot*(k-1) + 5)
   life(k) = Theta(pstar + pspot*(k-1) + 6)
   ingress(k) = Theta(pstar + pspot*(k-1) + 7)
   egress(k) = Theta(pstar + pspot*(k-1) + 8)
   IF( ingress(k) .LT. mingress ) THEN ! minimum ingress time
      ingress = mingress
   END IF
   IF( egress(k) .LT. mingress ) THEN ! minimum egress time
      egress = mingress
   END IF
 END DO
 ! macula defines the reference time = maximum spot-size time
 ! However, one can change the line below to whatever they wish.
 tref(:) = tmax(:)
 ! tcrit points = critical instances in the evolution of the spot
 DO k=1,Nspot
   tcrit1(k) = tmax(k) - 0.5D0*life(k) - ingress(k)
   tcrit2(k) = tmax(k) - 0.5D0*life(k)
   tcrit3(k) = tmax(k) + 0.5D0*life(k)
   tcrit4(k) = tmax(k) + 0.5D0*life(k) + egress(k)
 END DO
 ! temporal evolution of alpha
 DO i=1,ndata
   DO k=1,Nspot
     IF( t(i) .LT. tcrit1(k) .OR. t(i) .GT. tcrit4(k) ) THEN
       alpha(k,i) = 0.0D0
     ELSE IF( t(i) .LT. tcrit3(k) .AND. t(i) .GT. tcrit2(k) ) THEN
       alpha(k,i) = alphamax(k)
     ELSE IF( t(i) .LE. tcrit2(k) .AND. t(i) .GE. tcrit1(k) ) THEN
       alpha(k,i) = alphamax(k)*( ( t(i) - tcrit1(k) )/( ingress(k) ) )
     ELSE !IF( t(i) .LE. tcrit4(k) .AND. t(i) .GE. tcrit3(k) ) THEN
       alpha(k,i) = alphamax(k)*( ( tcrit4(k) - t(i) )/( egress(k) ) )
     END IF
     sinalpha(k,i) = DSIN(alpha(k,i))
     cosalpha(k,i) = DCOS(alpha(k,i))
   END DO
 END DO

 ! Lambda calculation
 DO i=1,ndata
   DO k=1,Nspot
     Lambda(k,i) = Theta(pstar+pspot*(k-1)+1) + 2.0D0*pi*(t(i)-tref(k))/Prot(k)
     sinLambda(k,i) = DSIN(Lambda(k,i))
     cosLambda(k,i) = DCOS(Lambda(k,i))
   END DO
 END DO

 ! beta calculation
 DO i=1,ndata
   DO k=1,Nspot
     cosbeta(k,i) = CosInc*SinPhi0(k) + SinInc*CosPhi0(k)*DCOS(Lambda(k,i))
     beta(k,i) = DACOS( cosbeta(k,i) )
     sinbeta(k,i) = DSIN(beta(k,i))
   END DO
 END DO

 ! =================================
 ! === SECTION 3: COMPUTING FMOD ===
 ! =================================

 ! zetapos and zetaneg
 DO i=1,ndata
   DO k=1,Nspot
     zetapos(k,i) = zeta(beta(k,i)+alpha(k,i))
     zetaneg(k,i) = zeta(beta(k,i)-alpha(k,i))
   END DO
 END DO

 ! Upsilon
 DO i=1,ndata
   DO k=1,Nspot
     DO n=0,4
       Upsilon(n+1,k,i) = zetaneg(k,i)**2 - zetapos(k,i)**2 &
                          + kronecker(zetapos(k,i),zetaneg(k,i))
       Upsilon(n+1,k,i) = (DSQRT(zetaneg(k,i)**(n+4)) &
                          - DSQRT(zetapos(k,i)**(n+4)))&
                          /Upsilon(n+1,k,i)
     END DO
   END DO
 END DO
        
 ! w
 DO i=1,ndata
   DO k=1,Nspot
     DO n=0,4
       w(n+1,k,i) = ( 4.0D0*(c(n+1)-d(n+1)*fspot(k)) )/( n+4.0D0 )
       w(n+1,k,i) = w(n+1,k,i)*Upsilon(n+1,k,i)
     END DO
   END DO
 END DO

 ! Area A
 DO i=1,ndata
   DO k=1,Nspot
     IF( alpha(k,i) .GT. tol ) THEN
       IF( beta(k,i) .GT. (halfpi+alpha(k,i)) ) THEN
         ! Case IV
         A(k,i) = 0.0D0
       ELSE IF( beta(k,i) .LT. (halfpi-alpha(k,i)) ) THEN
         ! Case I
         A(k,i) = pi*cosbeta(k,i)*sinalpha(k,i)**2
       ELSE
         ! Case II & III
         Psi(k,i) = DSQRT(1.0D0 - (cosalpha(k,i)/sinbeta(k,i))**2 )
         Xi(k,i) = sinalpha(k,i)*DACOS( -(cosalpha(k,i)*cosbeta(k,i))&
                   /(sinalpha(k,i)*sinbeta(k,i)) )
         A(k,i) = DACOS( cosalpha(k,i)/sinbeta(k,i) ) &
                         + Xi(k,i)*cosbeta(k,i)*sinalpha(k,i) &
                         - Psi(k,i)*sinbeta(k,i)*cosalpha(k,i)
       END IF
     ELSE
       A(k,i) = 0.0D0
     END IF
   END DO
 END DO

 ! q
 DO i=1,ndata
   DO k=1,Nspot
     q(k,i) = (A(k,i)*piI)*SUM(w(:,k,i))
   END DO
 END DO

 ! Fab0
 Fab0 = 0.0D0
 DO n=0,4
   Fab0 = Fab0 + (n*c(n+1))/(n+4.0D0)
 END DO
 Fab0 = 1.0D0 - Fab0

 ! Fab
 DO i=1,ndata
   Fab(i) = Fab0 - SUM(q(:,i))
 END DO

 ! Fmod
 DO i=1,ndata
   Fmod(i) = 0.0D0
   DO m=1,mmax
     Fmod(i) = Fmod(i) + U(m)*Box(m,i)*( Fab(i)/(Fab0*B(m))+(B(m)-1.0D0)/B(m) )
   END DO
 END DO






END SUBROUTINE macula

! ==============================================================================

! ==============================================================================
! =========================== SUBROUTINE: lnprior ==============================
! ==============================================================================

SUBROUTINE lnprior(lnpr, theta, lim, nt)
 implicit none

 INTEGER, INTENT(in) :: nt
 REAL(8), INTENT(in), DIMENSION(nt) :: theta
 REAL(8), INTENT(in), DIMENSION(nt,2) :: lim
 REAL(8), INTENT(out) :: lnpr
 REAL(8), PARAMETER :: inf = HUGE(1D0)
 INTEGER :: i

 lnpr = 0.0D0
 
 DO i=1,nt
 
  IF ( lim(i,1) .GT. theta(i) .OR. theta(i) .GT. lim(i,2) ) THEN
   lnpr = -inf
   EXIT
  END IF

 END DO    
    
END SUBROUTINE lnprior



! ==============================================================================
! =========================== SUBROUTINE: lnlikelihood =========================
! ==============================================================================
SUBROUTINE lnlikelihood(lnlike,y,yerr,model,C,ny)
  implicit none

  INTEGER, INTENT(IN) :: ny
  REAL(8), INTENT(IN) :: C
  REAL(8), INTENT(IN), DIMENSION(ny) :: y, yerr, model

  REAL(8), DIMENSION(ny) :: wdif
  
  REAL(8), INTENT(OUT) :: lnlike

  wdif = (y-model)/yerr

  lnlike = C-0.5D0*SUM(wdif*wdif)

END SUBROUTINE lnlikelihood
! ==============================================================================



! ==============================================================================
! =========================== SUBROUTINE: lnprobability=========================
! ==============================================================================

SUBROUTINE lnprobability(l,theta,x,y,yerr,LD,nspot,&
                         theta_inst,tstart,tend,lim,C,nt,nx)

 INTEGER, INTENT(IN) :: nt,nx,nspot
 REAL(8), INTENT(IN) :: C
 REAL(8), INTENT(IN), DIMENSION(nt) :: theta
 REAL(8), INTENT(IN), DIMENSION(nt,2) :: lim
 REAL(8), INTENT(IN), DIMENSION(nx) :: x,y,yerr
 REAL(8), INTENT(IN), DIMENSION(8) :: LD
 REAL(8), INTENT(IN), DIMENSION(2) :: theta_inst
 REAL(8), INTENT(IN), DIMENSION(1)  :: tstart, tend

 REAL(8), DIMENSION(8,nspot) :: theta_spot
 REAL(8), DIMENSION(12) :: theta_star
 REAL(8), DIMENSION(nx) :: Fmod
 REAL(8) :: lnlike, lnpr
 
 REAL(8), INTENT(OUT) :: l
 
 l = 0.0D0
 
 call lnprior(lnpr,theta,lim,nt)

 l = l + lnpr
 
 IF (l > -HUGE(1.0D0)) THEN
 
   theta_star(1:4) = theta(1:4)
   
   theta_star(5:12) = LD
   
   theta_spot = RESHAPE(theta(6:nt),(/8, nspot/))

   call macula(x,nx,nspot,1,theta_star,theta_spot,theta_inst,tstart,tend,Fmod)

   call lnlikelihood(lnlike, y, yerr, Fmod, C, nx)
   
   l = l + lnlike

 END IF
    
END SUBROUTINE lnprobability
! ==============================================================================



! ==============================================================================
! ================================= FUNCTION: ZETA =============================
! ==============================================================================
REAL(8) FUNCTION zeta(x)

implicit none

 REAL(8), INTENT(IN) :: x
 REAL(8), PARAMETER :: halfpi = 1.5707963267948966D0

 IF( x .LT. 0.0D0 ) THEN
   zeta = 1.0D0
 ELSE IF( x .GT. halfpi ) THEN
   zeta = 0.0D0
 ELSE !IF( x .GE. 0.0D0 .AND. x .LE. halfpi ) THEN
   zeta = DCOS(x)
 END IF

 END FUNCTION zeta
! ==============================================================================

! ==============================================================================
! =========================== FUNCTION: KRONECKER ==============================
! ==============================================================================
REAL(8) FUNCTION kronecker(x,y)

implicit none

 REAL(8), INTENT(IN) :: x, y

 IF( x .EQ. y ) THEN
   kronecker = 1.0D0
 ELSE
  kronecker = 0.0D0
 END IF

 END FUNCTION kronecker
! ==============================================================================



! ==============================================================================
END MODULE maculamod
